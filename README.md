# OctoPrint Add E1 to lasercut gcode Plugin

This is a small GCODE preprocessor that makes sure that uploaded GCODE file E1 at the end of each line that starts with G1.

When creating a GCODE file with Lightburn, Octoprint cannot estimate how long it will take to process this file.
I traced it down to the fact that Octoprint needs a E<number> at the end of each cut line to be able to calculate duration.
This plugin searches for each line that starts with G1 (a burn/cut command for the laser) and adds E1 at the end of such a line. 
