# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

import octoprint
import octoprint.plugin

class AddE1toLasercutGcodeStream(octoprint.filemanager.util.LineProcessorStream):
        def process_line(self, line):
                if (line.find(b'G1') == 0) and not line.__contains__(b'E'):
                        return line + b'E1'
                else:
                        return line

class AddE1toLasercutGcodePlugin(octoprint.plugin.OctoPrintPlugin):

	def add_e1_to_lasercut_gcode(self, path, file_object, links=None, printer_profile=None, allow_overwrite=True, *args, **kwargs):
		if not octoprint.filemanager.valid_file_type(path, type="gcode"):
			return file_object

		return octoprint.filemanager.util.StreamWrapper(file_object.filename, AddE1toLasercutGcodeStream(file_object.stream()))

__plugin_name__ = "AddE1toLasercutGcode"
__plugin_description__ = "Add E1 to every line that starts with G1 of a Lasercut GCODE files"
__plugin_pythoncompat__ = ">=2.7,<4"
__plugin_implementation__ = AddE1toLasercutGcodePlugin()
__plugin_hooks__ = {
	"octoprint.filemanager.preprocessor": __plugin_implementation__.add_e1_to_lasercut_gcode,
}
